import React,{useEffect,useState} from 'react'
import {SERVER_URL} from '../config.js'
import axios from 'axios'
let io = require('socket.io-client')
const socket = io(SERVER_URL);



function MyComponent() {
  const [SelectedStatus,setSelectedStatus] = useState('present')
  const [present,Setpresent] = useState([])
  const [history,setHistory] = useState([])

  const data = {
    status: ['present','past'],
    present: [
      {
        "username": "akhilkumar",
        "ts": new Date(),
        "avatar": null
      },
      {
        "username": "phani",
        "ts": new Date(),
        "avatar": null
      },
      {
        "username": "sailesh",
        "ts": new Date(),
        "avatar": null
      },
      {
        "username": "kiran",
        "ts": new Date(),
        "avatar": null
      },
      {
        "username": "raj",
        "ts": new Date(),
        "avatar": null
      }
    ],
    past: [
      {
        "username": "akhilkumar",
        "ts": new Date(),
        "avatar": null
      },
      {
        "username": "sailesh",
        "ts": new Date(),
        "avatar": null
      },
      {
        "username": "sailesh",
        "ts": new Date(),
        "avatar": null
      },
      {
        "username": "sailesh",
        "ts": new Date(),
        "avatar": null
      }
    ]


  }
  const renderUser = (each,index) => {
    if (index<3) {
      return (
        <div title={`${each.name}`} className="avatar" key={index}>
          <p >{each.name[0].toUpperCase()}</p>
        </div>
      )
    }
  }
  const onSelect = (e) => {
    setSelectedStatus(e.target.value)
  }

  const getHistory = async() => {
    let history = await axios.get(`${SERVER_URL}/dinoranger/api/v1/getHistory`)
    console.log('historyhistoryhistory',history);
    setHistory(history.data)
  }



  useEffect(() => {
    let userdata = JSON.parse(localStorage.getItem('userData'))
    userdata.isLive = true
    userdata.ts = new Date()
    socket.emit('new user',userdata);
    // socket emits live user
    socket.on('live users', function(msg){
     Setpresent(msg)
    });

    getHistory()
    GetCam()
    socket.on('live cam',function(stream) {
      const video = document.getElementById('videoConnected');
      console.log(stream);
      video.src = stream;
      video.play();
    })
  },[]);

  const GetCam = async() => {
    const video = document.getElementById('video');
    navigator.mediaDevices.getUserMedia({ video: true, audio: false })
      .then(function(stream) {
        // console.log(stream);
        socket.emit('live cam',stream);
          video.srcObject = stream;
          video.play();
      })
  };

  if (present) {
    return(
      <div className="main-container">
        <div className="view-container">
          <div className="main-visitors-container">
            <div className="visitors-container avatars">
            {
              present.length > 0 ?
              present.map((each,index)=>{
                return renderUser(each,index)
              })
            : (<div>no {SelectedStatus} users available</div>)
          }
          {
            present.length > 3 ?
              <div className="avatar">
                <p>{present.length- 3}</p>
              </div>
            : null
          }
            </div>
          </div>
          <div>
            <select className="select-status" onChange={(e)=>onSelect(e)}>
            {data.status.map((each,index)=>{
              return (<option key={index} value={each}>{each}</option>)
            })}
            </select>
          </div>
        </div>
        {SelectedStatus === 'past'?<div>
          {SelectedStatus}
          {history.length>0? history.map((each,index)=>{
            return (
              <div key={index}>
                <p key={index}>{each.name}</p>
                <p key={index}>{each.email}</p>
                <p key={index}>{each.ts}</p>
              </div>
            )
          }):null
        }
        </div>: null}
        <video id="video" width="640" height="480" autoplay></video>
        <video id="videoConnected" width="640" height="480" autoplay></video>
      </div>
    )
  }
}

export default MyComponent
