import React,{useState,useEffect} from 'react'
import axios from 'axios'
import {SERVER_URL} from '../config.js'
import {
  Link
} from "react-router-dom";


function LoginPage(props) {
  const [email,setEmail] = useState('');
  const [video,SetVideo] = useState();
  const [pass,setPass] = useState('')
  const _handleSubmit = async() => {
    let obj = {email,pass}
    try {
      let {data: {data}} = await axios.post(`${SERVER_URL}/dinoranger/api/v1/authLogin`,obj)
      localStorage.setItem('userData', JSON.stringify(data))
      props.history.push('/user')
    } catch (e) {
      console.log(e);
    }
  }

  useEffect(()=>{
    if (localStorage.getItem('userData')) {
      props.history.push('/user')
    }else {
      props.history.push('/')
    }
    // GetCam()

  },[])

  const GetCam = async() => {
    const video = document.getElementById('video');
    navigator.mediaDevices.getUserMedia({ video: true, audio: false })
      .then(function(stream) {
        console.log(stream);
          video.srcObject = stream;
          video.play();
      })
  };
  // console.log('data',video);

  return(
    <div>
      <button className="button-wrapper" onClick={()=> _handleHola()}>boom</button>
      <div className="form-container">
      <h3>Login</h3>
      <Link to="/register">Register</Link>
      <div className="pass-input">
        <label>password</label>
        <input className="input-wrapper" name="pass" placeholder="password" value={pass} onChange={(e) => setPass(e.target.value)} />
      </div>
          <div className="email-input">
            <label>Email</label>
            <input className="input-wrapper" name="email" placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)} />
          </div>
      </div>
    </div>
  )
}

export default LoginPage
