import React,{useState,useEffect} from 'react'
import axios from 'axios'
import {SERVER_URL} from '../config.js'

function UploadFiles() {
  const [file,setFile] = useState()
  const onClickHandler = async() => {
    const data = new FormData()
    data.append('file', file)
    try {
      const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        };
        console.log('data',data);
      await axios.post(`${SERVER_URL}/uploadFile`,data,config)
    } catch (e) {
      console.log(e);
    }
}

console.log(file,'asdadsadsad');
  return (
    <div>
      <input type="file" name="file" onChange={(file)=>setFile(file.target.files[0])} />
      <button onClick={()=>onClickHandler()}>upload</button>
    </div>
  )
}

export default UploadFiles
