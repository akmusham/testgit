import React,{useState} from 'react'
import axios from 'axios'
import {SERVER_URL} from '../config.js'

function RegisterPage(props) {
  const [email,setEmail] = useState('');
  const [pass,setPass] = useState('')
  const [name,setName] = useState('')
  const _handleSubmit = async() => {
    let obj = {email,pass,name}
    try {
      await axios.post(`${SERVER_URL}/dinoranger/api/v1/auth`,obj)
      props.history.push('/');
    } catch (e) {
      console.log(e);
    }

  }
  return(
    <div>
      <div className="form-container">
      <h3>register</h3>
        <input className="input-wrapper" name="name" placeholder="name" value={name} onChange={(e) => setName(e.target.value)} />
        <input className="input-wrapper" name="email" placeholder="email" value={email} onChange={(e) => setEmail(e.target.value)} />
        <input className="input-wrapper" type="password" name="pass" placeholder="password" value={pass} onChange={(e) => setPass(e.target.value)} />
        <button className="button-wrapper" onClick={()=> _handleSubmit()}>Submit</button>
      </div>
    </div>
  )
}

export default RegisterPage
